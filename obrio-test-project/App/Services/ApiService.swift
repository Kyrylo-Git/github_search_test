import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

protocol RequestProtocol {
    var urlScheme: String { get }
    var host: String { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var accept: String { get }
}

extension RequestProtocol {
    var urlScheme: String { "https" }
    var accept: String { "application/vnd.github.v3+json" }
    var host: String { "api.github.com" }
}


class APIService {
    private let apiRequestService = APIRequestService()
    private var requestBuilder = RequestBuilder()
    
    static let shared = APIService()
    
    private init() {}
    
    func getData<T>(for request: RequestProtocol, parsType: T.Type, params: [String: String], completion: ((Result<T?, Error>) -> ())?) where T: Codable {
        guard let apiRequest = requestBuilder.build(parametrs: request, params: params) else { return }
        apiRequestService.dataTask(with: apiRequest, parsType: parsType, completion: completion)
    }
    
}

class APIRequestService {
    private lazy var session = URLSession(configuration: configuration)
    private var task: URLSessionDataTask?
    
    private lazy var configuration: URLSessionConfiguration = {
        let conf = URLSessionConfiguration.default
        return conf
    }()
 
    func dataTask<T>(with request: URLRequest, parsType: T.Type, completion: ((Result<T?, Error>) -> Void)?) where T: Decodable {
        task = session.dataTask(with: request, completionHandler: { (data, response, error) in
            if let error = error { completion?(.failure(error)); return }
            do {
                guard let json = data else { completion?(.failure( CustomError.dataEmpty)); return }
                let object = try JSONDecoder().decode(parsType.self, from: json)
                completion?(.success(object))
            } catch {
                completion?(.failure(CustomError.encodeError("\(parsType)")))
            }
        })
        
        task?.resume()
    }
    
}

final class RequestBuilder {
    public func build<T: Codable>(parametrs: RequestProtocol, params: T) -> URLRequest? {
        let _url = getRequestURL(parametrs: parametrs, params: params as? [String: String])
        
        guard let url = _url else {
            fatalError("Could not create URL from components")
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = parametrs.httpMethod.rawValue
        
        setHeaders(request: &request, parametrs: parametrs)
        
        return request
    }
    
    private func getRequestURL(parametrs: RequestProtocol, params: [String: String]? = nil) -> URL?{
        var urlComponents = URLComponents()
        urlComponents.scheme = parametrs.urlScheme
        urlComponents.host = parametrs.host
        urlComponents.path = parametrs.path
        
        if parametrs.httpMethod == .get, let _params = params {
            urlComponents.queryItems = RequestParametersConfigurator.createQueryItemArray(from: _params)
        }
        
        return urlComponents.url
    }
    
    
    private func setHeaders(request: inout URLRequest, parametrs: RequestProtocol){
        request.setValue(parametrs.accept, forHTTPHeaderField: "Accept:")
    }
    
}

final class RequestParametersConfigurator {
    static func createQueryItemArray(from dictionary: [String: String]) -> [URLQueryItem]? {
        guard dictionary.keys.count > 0 else { return nil }
        var params: [URLQueryItem] = []
        dictionary.forEach{ (key, value) in
            params.append(URLQueryItem(name: key, value: value ))
        }
        return params
    }

}

enum CustomError: Error {
    case encodeError(String)
    case dataEmpty
}
