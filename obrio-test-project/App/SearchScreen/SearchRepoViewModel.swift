import Foundation

class SearchRepoViewModel {
    var endDownloadCompletion: ((DownloadProccess) -> ())?
    private var repositories = [RepoItem]() {
        didSet { repositories.sort { $0.stargazers_count > $1.stargazers_count } }
    }
    
    func getRepository(by name: String) {
        var detectedError: Error?
        let dispatchGroup = DispatchGroup()
        var tmpRepositories = [RepoItem]()
        
        endDownloadCompletion?(.startDownload)
        
        dispatchGroup.enter()
        getRepo(by: name, page: "1", perPage: Constants.elementsPerPage) { result in
            print("1: \(Thread.current)")
            defer { dispatchGroup.leave() }
            switch result {
            case.success(let object):
                object?.items.forEach {
                    tmpRepositories.append($0)
                }
            case .failure(let error):
                detectedError = error
            }
        }
        
        dispatchGroup.enter()
        getRepo(by: name, page: "2", perPage: Constants.elementsPerPage) { result in
            print("2: \(Thread.current)")
            defer { dispatchGroup.leave() }
            switch result {
            case.success(let object):
                object?.items.forEach {
                    tmpRepositories.append($0)
                }
            case .failure(let error):
                detectedError = error
            }
        }
                
        dispatchGroup.notify(queue: .main) { [weak self] in
            if let error = detectedError {
                self?.repositories = []
                self?.endDownloadCompletion?(.endDownload(.failure(error)))
                return
            }
            self?.repositories = tmpRepositories
            self?.endDownloadCompletion?(.endDownload(.success))
        }
    }
    
    private func getRepo(by name: String, page: String, perPage: String, _ closure: @escaping (Result<RepoObject?, Error>) -> Void) {
        let params = ["q": name,
                      "sort": "stars",
                      "page": page,
                      "per_page": perPage]
        
        APIService.shared.getData(for: SearchRequest.searchRepo, parsType: RepoObject.self, params: params) { result in
            closure(result)
        }
    }
    
}

extension SearchRepoViewModel: SearchRepoViewControllerDataSource {
    func getCellObject(for index: Int) -> RepoItemObjectCell? {
        repositories[index]
    }
    
    func numberOfRow() -> Int {
        repositories.count
    }

}

extension SearchRepoViewModel {
    private enum Constants {
        static let elementsPerPage = "15"
    }
}
