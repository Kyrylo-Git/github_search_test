import Foundation

enum SearchRequest: RequestProtocol {
    case searchRepo
    
    var path: String {
        switch self {
        case .searchRepo:
            return "/search/repositories"
        }
    }
    
    var httpMethod: HTTPMethod {
        switch self {
        case .searchRepo:
            return .get
        }
    }
    
}
