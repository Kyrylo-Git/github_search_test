import Foundation

struct RepoObject: Codable {
    var items: [RepoItem]
    
}

struct RepoItem: Codable, RepoItemObjectCell {
    var id: Int?
    var name: String?
    var language: String?
    var description: String?
    var stargazers_count: Int
    var html_url: String?
    
}

extension RepoItem {
    var getCellItems: [String?] {
        [name, language, description,
         String(stargazers_count), html_url]
    }
    
}

protocol RepoItemObjectCell {
    var getCellItems: [String?] { get }
}
