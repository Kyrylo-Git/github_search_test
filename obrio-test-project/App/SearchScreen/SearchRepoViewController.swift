import UIKit

protocol SearchRepoViewControllerDataSource: class {
    func getCellObject(for index: Int) -> RepoItemObjectCell?
    func numberOfRow() -> Int
}

class SearchRepoViewController: BasicViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var searchView: SearchView!
    
    private lazy var viewModel = SearchRepoViewModel()
    
    weak var dataSource: SearchRepoViewControllerDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        
        viewModel.endDownloadCompletion = { [weak self] in self?.downloadCompletion(step: $0)}
        dataSource = viewModel
        configureEmptyState()
    }
    
    private func setupUI() {
        searchView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "RepoCell", bundle: .main), forCellReuseIdentifier: "RepoCell")
    }
    
    private func configureEmptyState() {
        let emptyView = EmptyView()
        emptyView.configure(text: "Empty State")
        if dataSource?.numberOfRow() ?? 0 == 0 {
            tableView.backgroundView = emptyView
        } else {
            tableView.backgroundView = nil
        }
    }
    
    override func endDowloadCompletion(status: DownloadStatus) {
        switch status {
        case .success:
            tableView.reloadData()
        case .failure(let error):
            print(error)
        }
        configureEmptyState()
    }
    
}

extension SearchRepoViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dataSource?.numberOfRow() ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RepoCell", for: indexPath) as? RepoCell
        cell?.configure(with: dataSource?.getCellObject(for: indexPath.row))
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        UITableView.automaticDimension
    }
    
}

extension SearchRepoViewController: SearchViewDelegate {
    func searchButtonTapped(text: String) {
        viewModel.getRepository(by: text)
    }
    
}

enum DownloadProccess { case startDownload, endDownload(DownloadStatus) }
enum DownloadStatus { case success, failure(Error) }
