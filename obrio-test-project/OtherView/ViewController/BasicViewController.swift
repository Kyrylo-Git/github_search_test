import UIKit

class BasicViewController: UIViewController {
    
    private lazy var loadingView = LoadingView()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupLoadingView()
    }
    
    func downloadCompletion(step: DownloadProccess) {
        switch step {
        case .startDownload:
            addLoadingView()
        case .endDownload(let status):
            removeLoadingView()
            endDowloadCompletion(status: status)
        }
    }
    
    func endDowloadCompletion(status: DownloadStatus) {}
    
    func addLoadingView() {
        DispatchQueue.main.async {
            self.loadingView.addOn(view: self.view)
        }
    }
    
    func removeLoadingView() {
        DispatchQueue.main.async {
            self.loadingView.removeFromSuperview()
        }
    }
    
    private func setupLoadingView() {
        loadingView.frame = view.frame
        loadingView.backgroundColor = #colorLiteral(red: 0.4645321369, green: 0.4611783028, blue: 0.4671025872, alpha: 1)
    }
            
}
