import UIKit

class LoadingView: UIView {
    private var activityIndicator: UIActivityIndicatorView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialLayout()
    }
        
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func addOn(view: UIView) {
        self.activityIndicator.startAnimating()
        view.addSubview(self)
    }
    
    public override func removeFromSuperview() {
        activityIndicator.stopAnimating()
        super.removeFromSuperview()
    }
    
}

private extension LoadingView {
    func initialLayout() {
        activityIndicator = UIActivityIndicatorView(style: .large)
        addSubview(activityIndicator)
        
        [activityIndicator].forEach { $0?.translatesAutoresizingMaskIntoConstraints = false }
        
        NSLayoutConstraint.activate([
            activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
            activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
    }
    
}
