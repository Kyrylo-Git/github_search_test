import UIKit
 
protocol SearchViewDelegate: class {
    func searchButtonTapped(text: String)
}

class SearchView: XIBView {
    @IBOutlet private weak var textField: UITextField!
    @IBOutlet private weak var searchButton: UIButton!
    
    weak var delegate: SearchViewDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
       
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}

//MARK: - Actions
extension SearchView {
    @IBAction func searchButtonTapped(_ sender: Any) {
        guard searchButton.isEnabled, let text = textField.text else { return }
        endEditing(true)
        delegate?.searchButtonTapped(text: text)
    }
    
    
    @IBAction func textChange(_ sender: UITextField) {
        searchButton.isEnabled = textField.text?.count ?? 0 > 2
    }
    
}
