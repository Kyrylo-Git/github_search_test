import UIKit

class RepoCell: UITableViewCell {
    @IBOutlet private weak var conteinerStackView: UIStackView!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configure(with obj: RepoItemObjectCell?) {
        clearContainerStack()
        
        obj?.getCellItems.forEach {
            let label = UILabel()
            label.numberOfLines = 0
            label.text = $0
            conteinerStackView.addArrangedSubview(label)
        }
        
    }
    
    private func clearContainerStack() {
        conteinerStackView.subviews.forEach {
            $0.removeFromSuperview()
        }
    }
    
}
